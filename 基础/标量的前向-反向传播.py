import torch

# 前向传播
# 创建一个一维向量，其元素全为1
x = torch.ones(2)
# 自动计算x向量的微分值
x.requires_grad = True
z = x * 4
# 求z的2范数
y = z.norm()

# 反向传播
y.backward()

# 输出向量x的微分值
print(x.grad)

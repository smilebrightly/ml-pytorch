import torch

# 前向传播
# 创建一个2×1矩阵，矩阵中元素全为1
z = torch.ones(2, 1)
# 创建一个2×2矩阵
x = torch.Tensor([[2, 3], [1, 2]])
# 自动计算x矩阵的微分值
x.requires_grad = True

# 矩阵相乘，y = x × z
y = x.mm(z)

# 反向传播
y.backward(torch.ones(2, 1))

# 输出x的微分值
print(x.grad)
